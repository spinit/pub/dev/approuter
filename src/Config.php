<?php

namespace Spinit\Dev\AppRouter;

use Spinit\Dev\AppRouter\Data;
use Spinit\Util\Error\NotFoundException;

class Config {
    private static $managerList;

    private static $instanceList;

    public static function addManager($className, $info) {
        if (!self::$managerList) self::$managerList = new Data();
        if (is_array($info)) $info = new Data($info);
        self::$managerList[$className] = $info;
    }
    public static function getManager($name) {
        if (!self::$managerList) self::$managerList = new Data();
        if (!self::$managerList->offsetExists($name)) {
            throw new NotFoundException();
        }
        return self::$managerList[$name];
    }
    public static function getManagerList() {
        if (!self::$managerList) self::$managerList = new Data();
        return self::$managerList;
    }

    public static function addInstance($name, $info) {
        if (!self::$instanceList) self::$instanceList = new Data();
        if (is_array($info)) $info = new Data($info);
        self::$instanceList[$name] = $info;
    }
    public static function getInstance($name) {
        if (!self::$instanceList) self::$instanceList = new Data();
        if (!self::$instanceList->offsetExists($name)) {
            throw new NotFoundException();
        }
        return self::$instanceList[$name];
    }
    public static function getInstanceList() {
        if (!self::$instanceList) self::$instanceList = new Data();
        return self::$instanceList;
    }
}