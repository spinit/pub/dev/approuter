<?php
namespace Spinit\Dev\AppRouter;

use Spinit\Dev\AppRouter\Helper\ChannelInterface;
use Spinit\Dev\AppRouter\Helper\ChannelStd;
use Spinit\Dev\AppRouter\Helper\ChannelTest;
use Webmozart\Assert\Assert;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getenv;

class Response {

    private $_content;
    private $_headers;
    private $_data = [];
    private $_code;

    private $_thens = [];

    public function __construct($status = 0)
    {
        $this->setStatusCode($status ?:200);
        $this->setContent(function($data) {
            return json_encode($data);
        }, 'application/json; charset=utf-8');
        $this->_data = new Data();
    }

    public function setContent(callable $caller, $ctype = '') {
        if ($ctype) {
            $this->setHeader('Content-Type', $ctype);
        }
        $this->_content = function ($data, $channel) use ($caller) {
            return call_user_func($caller, $data, $channel);
        };
        return $this;
    }

    public function getContent($channel = null) {
        return call_user_func($this->_content, $this->_data, $channel);
    }

    public function setStatusCode($code) {
        $this->_code = $code;
    }
    
    public function getStatusCode() {
        return $this->_code;
    }
    
    public function setHeader($name, $value) {
        return $this->setHeaders($name, [$value]);
    }

    public function setHeaders($name, $value) {
        $this->_headers[$name] = $value;
        return $this;
    }

    public function getHeaders() {
        $args = func_get_args();
        
        if (count($args) == 0) return $this->_headers;

        return arrayGet($this->_headers, $args[0], []);
    }

    public function getHeader($name) {
        // return first value 
        $value = '';
        foreach($this->getHeaders($name) as $value) {
            $value = $value;
            break;
        }
        return $value;
    }

    public function send(ChannelInterface $channel = null) {

        $channel = $channel ?: new ChannelStd();
        $channel->open();
        try {
            // serializzazione nel canale
            $content = $this->getContent($channel);
            $channel->header($this->getStatusCode(), $this->_headers);
            $channel->write($content);
            
        } catch (ResponseException $e) {
            // non fare nulla ... ha già fatto il gestore del contenuto
        }
        $channel->close();
        // codice da eseguire come demone una volta inviata la risposta
        foreach($this->_thens as $then) {
            call_user_func($then);
        }
}

    /**
     * @codeCoverageIgnore
     */    
    public function __toString() {
        $result = '';
        try {
            $this->send();
        } catch (\Exception $e) {
            $result = json_encode([
                'message' => $e->getMessage(),
                'trace'=>explode("\n", $e->getTraceAsString())
            ], JSON_PRETTY_PRINT);
        }
        return $result;
    }
    
    public function stop() {
        throw new ResponseException($this);
    }

    public function set($data) {
        $args = func_get_args();
        call_user_func_array([$this->_data, 'set'], $args);
        return $this;
    }
    public function get() {
        $args = func_get_args();
        if (!count($args)) return $this->_data;
        if (count($args)==1) $args[] = '';
        return call_user_func_array([$this->_data, 'get'], $args);
    }
    public function add($name, $data) {
        call_user_func([$this->_data, 'add'], $name, $data);
        return $this;
    }
    
    public function addExec() {
        return call_user_func_array([$this, 'add'], ['data,exec', func_get_args()]);
    }
    public function addCommand() {
        $args = func_get_args();
        array_unshift($args, 'cmd');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    public function addContent() {
        $args = func_get_args();
        array_unshift($args, "desktop.setContent('[id=\"'+args[0]+'\"]', args[1])");
        return call_user_func_array([$this, 'addCommand'], $args);
    }
    /**
     * @codeCoverageIgnore
     */
    public function setType($name) {
        switch($name) {
            case 'html' : $type = 'text/html; charset=UTF-8'; break;
            case 'xml' : $type = 'text/xml; charset=UTF-8'; break;
            case 'text' : $type = 'text/plain; charset=UTF-8'; break;
            default : $type = getMimeType($name); break;
        }
        $this->setHeader('Content-Type', $type);
        return $this;
    }

    public function then(callable $then) {
        // viene chiesto di eseguire un processo dopo la chiusura del canale ... allora occorre sospendere il limite temporale
        set_time_limit(0);
        $this->_thens[] = $then;
    }
}
