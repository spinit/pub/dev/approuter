<?php
namespace Spinit\Dev\AppRouter;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

class Request {
    /**
     * @var Data
     */
    private $_data;

    private $_method;
    private $_path;
    private $_basePath;
    private $_scheme = 'http';

    private function parsePath(string $path) {
        $part = explode('://', $path);
        if (count($part)<2) array_unshift($part, $this->_scheme);
        return $part;
    }

    public function __construct(string $method, $path, Data $data = null) 
    {
        if (is_string($path)) {
            list($this->_scheme, $path) = $this->parsePath($path);
        }
        $this->_data = $data ? $data : new Data();
        foreach(['query', 'post', 'cookie', 'files', 'app', 'headers'] as $sec) {
            if (!$this->_data->get($sec)) $this->_data->set($sec, []);
        }
        $this->setMethod($method);
        $this->setPath($path);
    }
    
    public function dup() {
        return (new self($this->getMethod(), [$this->_basePath, $this->_path], $this->_data))
               ->setScheme($this->_scheme);
    }

    public function getScheme() {
        return $this->_scheme;
    }
    public function setScheme($scheme) {
        $this->_scheme = $scheme;
        return $this;
    }

    public function getMethod() {
        return $this->_method;
    }
    public function setMethod($data) {
        $this->_method = $data;
        return $this;
    }
    
    public function getPath() {
        $args = func_get_args();
        if (count($args) == 0) {
            return implode('/', $this->_path);
        }
        return arrayGet($this->_path, $args[0]);
    }

    public function getUrl($withScheme = true) {
        return ($withScheme ? $this->getScheme().'://':'//').$this->getBasePath().$this->getPath();
    }

    public function getBasePath() {
        return implode('/', $this->_basePath);
    }

    public function setPath($newpath) {
        if (is_string($newpath)) {
            $part = explode('?', $newpath);
            $this->_path = asArray(array_shift($part), '/');
            $this->_basePath = [];
            parse_str(array_shift($part), $get);
            $this->setQuery($get);
        } else if (is_array($newpath)) {
            list($base, $path) = $newpath;
            $this->_path = asArray($path, '/');
            if ($base == '/') throw new \Exception();
            $this->_basePath = asArray($base, '/');
        }
        return $this;
    }

    public function shiftPath() {
        $path = $this->_path;
        $base = $this->_basePath;
        if (count($path)) {
            $result = array_shift($path);
            $base []= $result;
        }
        return (new self($this->getMethod(), [$base, $path], $this->_data))->setScheme($this->_scheme);
    }

    public function countPath() {
        return count($this->_path);
    }

    protected function set($base, $data) {
        $args = func_get_args();
        $keys = [$base];
        if (count($args)>2) {
            $keys = array_merge($keys, asArray($data, ','));
            $data = $args[2];
        }
        $this->_data->set($keys, $data);
        return $this;
    }

    public function setQuery($data) {
        $args = array_merge(['query'], func_get_args());
        return call_user_func_array([$this, 'set'], $args);
    }

    public function getQuery($name = '') {
        $keys = array_merge(['query'], asArray($name, ','));
        return $this->_data->get($keys);
    }

    public function hasQuery($name) {
        $keys = array_merge(['query'], asArray($name, ','));
        return $this->_data->has($keys);
    }

    public function setPost($data) {
        $args = array_merge(['post'], func_get_args());
        return call_user_func_array([$this, 'set'], $args);
    }
    public function getPost($keys = '') {
        $keys = array_merge(['post'], asArray($keys, ','));
        return $this->_data->get($keys)?:($keys?'':[]);
    }
    public function hasPost($keys = '') {
        $keys = array_merge(['post'], asArray($keys, ','));
        return $this->_data->has($keys);
    }

    public function setCookie($data) {
        $args = func_get_args();
        if (count($args) > 2) {
            $name = $args[0];
            $value = $args[1];
            $path = $args[2];
            $ret = setcookie($name, $value, ['expires'=>0, 'path'=>$path, 'samesite' => 'Lax']);
            if (!$ret ) {
                $ret = setcookie($name, $value, 0, $path);
            }
        }
        $args = array_merge(['cookie'], $args);
        return call_user_func_array([$this, 'set'], $args);
    }
    public function getCookie($keys = '') {
        $keys = array_merge(['cookie'], asArray($keys, ','));
        return $this->_data->get($keys);
    }
    public function haCookie($key) {
        $key = array_merge(['cookie'], asArray($key, ','));
        return $this->_data->has($key);
    }

    public function setFiles($data) {
        $args = array_merge(['files'], func_get_args());
        return call_user_func_array([$this, 'set'], $args);
    }
    public function getFiles($keys = '') {
        $keys = array_merge(['files'], asArray($keys, ','));
        return $this->_data->get($keys);
    }

    public function setApp($data) {
        $args = array_merge(['app'], func_get_args());
        return call_user_func_array([$this, 'set'], $args);
    }
    public function getApp($keys = '') {
        $keys = array_merge(['app'], asArray($keys, ','));
        return $this->_data->get($keys);
    }
    public function hasApp($key) {
        $key = array_merge(['app'], asArray($key, ','));
        return $this->_data->has($key);
    }

    public function setHeaders($data) {
        $args = array_merge(['headers'], func_get_args());
        return call_user_func_array([$this, 'set'], $args);
    }
    public function getHeaders($keys = '') {
        $keys = array_merge(['headers'], asArray($keys, ','));
        return $this->_data->get($keys);
    }
    public function hasHeaders($key) {
        $key = array_merge(['headers'], asArray($key, ','));
        return $this->_data->has($key);
    }
}