<?php

namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\InstanceInterface;

use function Spinit\Util\arrayGetAssert;

trait HasInstanceTrait {

    /**
     * @var InstanceInterface
     */
    private $_instance;

    protected function setInstance(InstanceInterface $instance) {
        $this->_instance = $instance;
        return $this;
    }

    public function getInstance() : InstanceInterface {
        return $this->_instance;
    }
    public function getDataSource($name = '') {
        return $this->getInstance()->getDataSource($name);
    }
}