<?php

namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Dev\AppRouter\getMimeType;
use function Spinit\Util\arrayGet;
use function Spinit\Util\file_get_contents;

class Asset {

    use HasInstanceTrait;

    private $root;

    public function __construct(InstanceInterface $instance, $root)
    {
        $this->setInstance($instance);
        $this->root = $root;
    }

    public function run(string $path, string $event = '', Request $request = null, $processor = null) {
        $fname = fspath($this->root, $path);
        $info = pathinfo($fname);
        
        $response = $this->getInstance()->getResponse();

        if (arrayGet($info, 'extension') != 'php' and  is_file($fname)) {
            $response->setHeader('Content-Type',getMimeType($info['extension']));
            $response->setContent(function($data, $channel) use ($fname, $response, $processor) {
                // invio gli headers già impostati
                $channel AND $channel->header($response->getStatusCode(), $response->getHeaders());
                if (!$processor) {
                    readfile($fname);
                } else {
                    echo $processor(file_get_contents($fname));
                }
                $response->stop();
            });
    
        } else {
            return $this->exec($fname, $event, $request);
        }
        return $response;
    }

    private function exec(string $file, string $event, Request $request = null) {
        $response = $this->geTInstance()->getResponse();
        try {
            ob_start();
            list($script, $args) = $this->getPathScript($file, $event);
            $ret = require $script;
        } finally {
            $cnt = ob_get_clean();
        }
        
        if (is_array($ret)) {
            $response->set($ret);
        }
        if ($cnt) {
            $response->setContent(function ($data) use ($cnt) {
                return $cnt;
            });
        }
        // quello che è stato stampato viene perso
        return $response;
    }

    private function getPathScript($file, $event) {
        $args = [];
        $script = $fname = $file;
        if (substr($script, -4)!= '.php') {
            while (strlen($fname) >= $this->root) {
                $script = fspath($fname, 'index.php');
                if (is_file($script)) {
                    // controllare che ci sia event
                    if ($event) {
                        $script = fspath($fname, '_'.$event.'.php');
                    }
                    break;
                }
                if ($fname == $this->root) break;
                array_unshift($args, basename($fname));
                $fname = dirname($fname);
            }
        }
        if (!is_file($script)) {
            throw new NotFoundException('File not found : '.$script.' ['.$fname.']');
        }
        return [$script, $args];
   }
   public function makePath($path) {
        $fname = fspath($this->root, $path);
        if (is_file($fname)) {
            $path .= '?_sha='.sha1_file($fname);
        } else {
            //var_dump([$fname, is_file($fname)]);
        }
        return $path;
    }

}
