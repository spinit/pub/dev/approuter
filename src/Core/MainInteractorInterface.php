<?php
namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\Request;

interface MainInteractorInterface {
    /**
     * @var array $managers lista dei manager da installare
     * @var array $admin credenziali di accesso principali dell'amministratore
     */
    public function install(iterable $instanceList);
    public function initRoute(AddRouteInterface $main);
    public function publish(string $topic, array $param, $realm = '');
    public function getConsole();
    public function getHome();
    public function decrypt($token);
    public function encrypt($data);
    public function getUser(?Request $request);
    public function setUser(?Request $request, $user, $path);
    public function searchUser($id_url, $name, $passwd, $ds = null);
}