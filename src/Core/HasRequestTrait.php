<?php

namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\Request;
use Webmozart\Assert\Assert;

trait HasRequestTrait {

    /**
     * @var Request
     */
    private $_request;

    public function setRequest(Request $request) {
        $this->_request = $request;
        return $this;
    }

    public function getRequest() : ?Request {
        return $this->_request;
    }
}