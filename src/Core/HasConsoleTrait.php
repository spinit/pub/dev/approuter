<?php 
namespace Spinit\Dev\AppRouter\Core;

trait HasConsoleTrait {
    private $_console;

    public function setConsole(ConsoleInterface $console) {
        $this->_console = $console;
        return $this;
    }

    public function getConsole() {
        if (!$this->_console) $this->_console = new Console();
        return $this->_console;
    }
}