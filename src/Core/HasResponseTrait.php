<?php

namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\Response;

trait HasResponseTrait {

    /**
     * @var Response
     */
    private $_response;

    protected function setResponse(Response $response) {
        $this->_response = $response;
        return $this;
    }

    public function getResponse() : Response {
        if (!$this->_response) $this->setResponse(new Response());
        return $this->_response;
    }
}