<?php

namespace Spinit\Dev\AppRouter\Core;

interface ConsoleInterface {
    function log();
    function error();
    function info();
}