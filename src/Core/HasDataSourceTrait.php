<?php

namespace Spinit\Dev\AppRouter\Core;

use function Spinit\Util\arrayGetAssert;

trait HasDataSourceTrait {

    private $_list = [];

    public function setDataSource($name, $DS) {
        $this->_list[$name] = $DS;
    }

    public function getDataSource($name = '') {
        return arrayGetAssert($this->_list, $name, 'DataSource non impostato', 100);
    }

    public function getDataSourceList() : iterable {
        return $this->_list;
    }
}