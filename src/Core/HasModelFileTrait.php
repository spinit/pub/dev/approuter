<?php
namespace Spinit\Dev\AppRouter\Core;

use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Spinit\Lib\Model\Adapter\Xml\ModelAdapterXml;
use Spinit\Lib\Model\Model;

use function Spinit\Dev\AppRouter\fspath;

trait HasModelFileTrait {

    private function getModel($root, $name, $ds) {
        $fname = fspath($root, $name.'.xml');
        return new Model(new ModelAdapterXml($name, $fname), $ds);
    }

    private function getModels($root, $ds) {
        $fs = new Filesystem(new LocalFilesystemAdapter($root));
        foreach($fs->listContents('.')->filter(function($item) {
            return $item->isFile() and pathinfo($item->path(), PATHINFO_EXTENSION) == 'xml';
        }) as $item) {
            $name = substr($item->path(), 0, -4);
            $fname = fspath($root, $item->path());
            yield new Model(new ModelAdapterXml($name, $fname), $ds);
        }
    }    
}