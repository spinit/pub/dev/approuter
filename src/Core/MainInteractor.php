<?php
namespace Spinit\Dev\AppRouter\Core;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\WampService;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\getenv;

abstract class MainInteractor implements MainInteractorInterface {

    private $_home;
    public function publish($topic, $param = [], $realm = 'realm1') {
    
        $service = new WampService($this->getWampServerUrl());
        return $service->publish($topic, $param, $realm);
    }
    protected function getWampServerUrl() {
        return getenv('WAMP_SERVER');
    }

    public function setHome($home) {
        $this->_home = $home;
    }
    public function getHome($path = '') {
        return fspath($this->_home, $path);
    }
    public function decrypt($token) {
        $secret = getenv('MAIN_SECRET_KEY') ?: md5(__FILE__.sha1_file(__FILE__));
        $part = explode(':', $token);
        $encrypt = array_shift($part);
        $iv = array_shift($part);
        return @openssl_decrypt($encrypt, 'aes-256-cbc', $secret, 0, base64_decode($iv));
    }
    
    public function encrypt($data) {
        $secret = getenv('MAIN_SECRET_KEY') ?: md5(__FILE__.sha1_file(__FILE__));
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypt = @openssl_encrypt ($data, 'aes-256-cbc', $secret, 0, $iv);
        return $encrypt.':'.base64_encode($iv);
        
    }

    public function getUser(?Request $request) {
        Assert::notNull($request, 'Richiesta non impostata');
        $token = $request->getCookie('token-user');
        $user = json_decode($this->decrypt($token), true);
        return $user;
    }

    public function setUser(?Request $request, $user, $path) {
        Assert::notNull($request, 'Richiesta non impostata');
        $token = $this->encrypt(json_encode($user));
        $request->setCookie('token-user', $token, $path);
    }

    public function searchUser($id_url, $name, $passwd, $ds = null) {
        return null;
    }
}