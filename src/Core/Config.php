<?php
namespace Spinit\Dev\AppRouter\Core;
 
use Spinit\Dev\AppRouter\InstanceInterface;

class Config {
    use HasInstanceTrait;

    public function __construct(InstanceInterface $instance)
    {
        $this->setInstance($instance);
    }
    public function get($name, $default) {
        return $default;
    }
}