<?php

namespace Spinit\Dev\AppRouter\Core;

interface AddRouteInterface {
    public function addRoute($prefix, $initer);
}