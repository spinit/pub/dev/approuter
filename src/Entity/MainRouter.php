<?php

namespace Spinit\Dev\AppRouter\Entity;

use Spinit\Dev\AppRouter\AppRouter;
use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Core\RouteNotFoundException;
use Spinit\Dev\AppRouter\Instance\FsInstance;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\ResponseException;

use function Spinit\Dev\AppRouter\debug;

class MainRouter implements AddRouteInterface {

    /**
     * @var AppRouter
     */
    private $router;

    /**
     * @var MainInteractorInterface
     */
    private $interactor;

    public function __construct(MainInteractorInterface $interactor = null) {
        $this->router = new AppRouter();
        $this->interactor = $interactor;
    }
    
    public function getInteractor() {
        return $this->interactor;
    }

    public function addRoute($prefix, $executor) {
        $this->router->addRoute($prefix, $executor);
    }

    public function run(Request $request) {
        
        if ($this->interactor) {
            $this->interactor->initRoute($this);
        }
        if ($this->router->isEmpty()) {
            return $this->execInstance($this->getInstaller(), $request);
        }

        try {

            // default activity
            return $this->router->run($request);

        } catch (RouteNotFoundException $e) {
            return $this->execInstance(new FsInstance( __DIR__.'/../../template/not-found', $this->interactor), $request);
        } catch (ResponseException $e) {
            /**
             * @codeCoverageIgnore
             */
            return $e->getResponse();
        }
    }

    private function getInstaller() {
        $instance = new FsInstance( __DIR__.'/../../template/install', $this->interactor);
        return $instance;
    }

    private function execInstance($instance, Request $request) {
        $newRequest = $request;
        $base = $request->getBasePath();
        if (!$base) {
            $newRequest = $request->shiftPath();
            $base = $newRequest->getBasePath();
        }
        $instance->setBasePath($base);
        return $instance->run($newRequest);
    }
}