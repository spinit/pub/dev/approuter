<?php

namespace Spinit\Dev\AppRouter\Entity;

use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

/**
 * @codeCoverageIgnore
 */
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
           $headers = [];
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }
}

/**
 * @codeCoverageIgnore
 */
class MainStandard {
    private $main;

    public function __construct(MainInteractorInterface $interactor = null) {
        $this->main = new MainRouter($interactor);
    }

    public function getInteractor() {
        return $this->main->getInteractor();
    }
    public function run() {
        $startTime = microtime(true);
        $request = new Request(
            $_SERVER['REQUEST_METHOD'], 
            $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
        );
        $request->setQuery($_GET);
        $request->setApp(arrayGet($_POST, '_', []));
        if (isset($_POST['_'])) {
            unset($_POST['_']);
        }
        $request->setPost($_POST);
        $request->setCookie($_COOKIE);
        $request->setFiles($_FILES);
        $request->setHeaders(getallheaders());
        $response = $this->main->run($request);
        if ($response instanceof Response) {
            $response->set('execTime', round(microtime(true) - $startTime, 4));
        }
        return $response;
    }
}
