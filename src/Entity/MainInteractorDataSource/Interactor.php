<?php

namespace Spinit\Dev\AppRouter\Entity\MainInteractorDataSource;

use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\HasConsoleTrait;
use Spinit\Dev\AppRouter\Core\HasDataSourceTrait;
use Spinit\Dev\AppRouter\Core\MainInteractor;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Lib\DataSource\DataSourceInterface;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\loadConfig;
use function Spinit\Util\arrayGet;
use function Spinit\Util\getInstance;
use function Spinit\UUIDO\randHexToday;

class Interactor extends MainInteractor {

    use HasConsoleTrait;
    use HasDataSourceTrait;

    public function __construct(DataSourceInterface $ds, $home = "") {
        $this->setDataSource('', $ds);
        $this->setHome($home);
    }

    // il primo parametro è :
    // - o la configurazione dell'istanza da caricare
    // - o l'id della url da caricare
    // il secondo parametro è l'id dell'istanza da caricare ... viene presa la prima
    public function getInstance($query) {
        Assert::notEmpty($query);
        if (is_array($query)) {
            $conf = $query;
        } else {
            $conf = $this->getInstanceUrlList('', $query)->first();
        }
        if (!$conf) debug($conf, $query);
        Assert::isArray($conf);
        $instance = getInstance(
            arrayGet($conf,'manager'),   // classe da instanziare
            $this,
            arrayGet($conf,'conf'),  // stringa di configurazione
            arrayGet($conf,'init'),     // parametri di implementazione
            $conf
        );
        $instance->setDataSource('main', $this->getDataSource());
        $instance->setDataSource('', new DataSource($conf['datasource']));
        $instance->setBasePath($conf['url']);
        return $instance;
    }

    public function initRoute(AddRouteInterface $main)
    {
        foreach($this->getInstanceUrlList() as $conf) {
            $main->addRoute($conf['url'], function($request) use ($conf) {
                $instance = $this->getInstance($conf);
                // request principale
                $instance->setRequest($request);
                // request di lavoro ... può essere "consumata" dai runner
                return $instance->run($request->dup());
            });
        }
    }

    public function install(iterable $instanceList)
    {
        (new Installer($this))->exec($instanceList);
    }

    public function getInstanceUrlList($id_ice = '', $id_url = '') {
        $data = [];
        $ds = $this->getDataSource();
        $selector = '';
        $arg = [];
        if ($id_url) {
            $selector = 'and uu.id = {{@id}}';
            $arg = ['id'=>$id_url];
        } else if ($id_ice) {
            $selector = 'and ii.id = {{@id}}';
            $arg = ['id'=>$id_ice];
        }
        if ($ds->check('osx_ice_url')) {
            $data = $ds->query ("
                select hex(uu.id) as id, 
                       hex(uu.id_ice) as id_ice, 
                       uu.url, 
                       ip.manager,
                       ii.home,
                       ii.name,
                       ip.init,
                       ip.conf, 
                       ip.theme,
                       ii.conf as datasource 
                from osx_ice_url uu 
                inner join osx_ice ii on (uu.id_ice = ii.id {$selector})
                -- se viene individuato un alias allora i dati principali sono del parent
                inner join osx_ice_url ip on (ifnull(uu.id_par, uu.id) = ip.id and ip.act = '1')
                where uu.act = '1'
            ", $arg);
        }
        return $data;
    }

    public function searchUser($id_url, $login, $passwd, $dsPar = null) {
        $ds = null;
        if (is_callable($dsPar)) {
            try {
                $ds = call_user_func($dsPar);
            } catch (\Exception $e) {}
        } else {
            $ds = $dsPar;
        }
        $user = ($ds ?: $this->getDataSource())->select(
            'osy_ang', 
            [
                'aut_lgn'=>$login, 
                'aut_act'=>'1',
                'id_url' =>  $id_url,
                'dat_del__'=>null
            ], 
            'id, id_url, nme, aut_lgn, aut_pwd, aut_role, aut_info')->first();
        if ($user and $user['aut_pwd'] == md5($passwd)) {
            unset ($user['aut_pwd']);
            $user['id_ses'] = randHexToday(32);
            return $user;
        }
        return null;
    }
}
