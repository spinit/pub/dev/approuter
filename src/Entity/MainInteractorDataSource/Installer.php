<?php
namespace Spinit\Dev\AppRouter\Entity\MainInteractorDataSource;

use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Spinit\Dev\AppRouter\Core\HasModelFileTrait;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Lib\Model\Adapter\Xml\ModelAdapterXml;
use Spinit\Lib\Model\Model;
use Spinit\UUIDO\Maker;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\arrayGetAssert;
use function Spinit\Util\getenv;
use function Spinit\Util\getInstance;
use function Spinit\UUIDO\randHex;

class Installer {
    /**
     * @var Interactor
     */
    private $_interactor = null;
    const SEMAPHORE = '/tmp/osy-installer.semaphore';
    
    use HasModelFileTrait;

    public function __construct(Interactor $interactor) {
        $this->_interactor = $interactor;
    }

    public function exec(iterable $instanceList) {
        set_time_limit(0);
        // creazione struttura dati sul database principale
        $ds = $this->_interactor->getDataSource();
        Assert::notNull($ds, 'DataSource non impostato');
        foreach($this->getModels(fspath(__DIR__, 'ModelMain'), $ds) as $model) {
            $this->getConsole()->log('Main : Model : '.$model->getResource());
            $model->init();
        }
        // creazione istanze
        foreach($instanceList as $instance) {
            $this->createInstance($ds, $instance);
        }
    }

    public function getConsole() {
        return $this->_interactor->getConsole();
    }

    private function createInstance($ds, $instance) {
        $name = arrayGet($instance, 'name');
        $home = arrayGet($instance, ['info', 'home'], $name);
        $datasource = arrayGetAssert($instance, 'datasource', 'DataSource non impostato');
        $managers = arrayGetAssert($instance, 'managerList', 'Nessun Manager Impostato');
        // creazione struttura principale
        $ice = [
            'id'    =>  randHex(10),
            'name'  =>  $name,
            'home'  =>  $home,
            'conf'  =>  $datasource,
            'act'   =>  '1'
        ];
        //  test instance connection
        $dsi = new DataSource($ice['conf'].":1");
        $dsi->setCounter(new Maker($ice['id']));
        // configurazione dell'istanza creata
        foreach($this->getModels(fspath(__DIR__, 'ModelInstance'), $dsi) as $model) {
            $this->getConsole()->log('Instance : Model : '.$model->getResource());
            $model->init();
        }
        
        $this->getConsole()->log('Init instance : '.$name);
        $ds->insert('osx_ice', $ice);
        foreach($managers as $manager) {
            $this->createManager($dsi, $ice, $manager);
        }
        return $ice;
    }

    private function createManager($ds, $ice, $manager) {
        $rec = [
            'id_ice'=>  $ice['id'],
            'manager'  =>  arrayGet($manager, ['manager']),
            'url' =>  arrayGet($manager, ['prefix']),
            'theme' =>  arrayGet($manager, ['theme']),
            'conf'  =>  arrayGet($manager, ['conf']),
            'init'  =>  arrayGet($manager, ['init']),
            'act'   =>  '1'
        ];
        $admin = arrayGet($manager, 'admin', false);
        
        $instanceManager = getInstance($rec['manager'], $this->_interactor, $rec['conf'], $rec['init'], $rec);
        $instanceManager->setDataSource('main', $this->_interactor->getDataSource());
        $instanceManager->setDataSource('', $ds);
        $rec['id'] = (string) $instanceManager->getDataSource()->getCounter()->next();
        if ($admin) {
            // vuole che venga impostato il manager come primo utente
            $anag = [
                'id_url'=>$rec['id'],
                'nme'    => $admin['name'],
                'nme_lst'=> $admin['name'],
                'aut_lgn'=> $admin['name'],
                'aut_pwd'=> $admin['passwd'],
                'aut_act'=>'1',
            ];
            $model = $this->getModel(fspath(__DIR__, 'ModelInstance'), 'Anag', $instanceManager->getDataSource());
            $model->set($anag);
            $model->save();

        }
        $this->getConsole()->log('Install Manager : '.get_class($instanceManager));
        $instanceManager->install($model /* record admin */);
        $instanceManager->getDataSource('main')->insert('osx_ice_url', $rec);
        return $rec;
    }
}