<?php

namespace Spinit\Dev\AppRouter\Entity;

use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\arrayGetAssert;
use function Spinit\Util\getInstance;
use function Spinit\UUIDO\randHex;

use League\Flysystem\Filesystem;
use Spinit\Dev\AppRouter\Console;
use Spinit\Dev\AppRouter\Core\HasConsoleTrait;
use Spinit\Dev\AppRouter\Core\HasModelFileTrait;
use Spinit\Dev\AppRouter\Core\MainInteractor;
use Spinit\Dev\AppRouter\WampService;
use Spinit\Lib\DataSource\DataSource;
use Spinit\UUIDO\Maker;

class MainInteractorJson extends MainInteractor {
    
    /**
     * @var array
     */
    private $_data = [];
    private $_fs;
    private $_fname;
    private $_console;

    use HasModelFileTrait;
    use HasConsoleTrait;

    public function __construct(string $fname, Filesystem $fs)
    {
        $this->_fs = $fs;
        $this->_fname = $fname;
    }

    private function initData() {
        if ($this->_fs->fileExists($this->_fname)) {
            $this->_data = json_decode($this->_fs->read($this->_fname), true) ?: [];
        }
    }
    public function initRoute(AddRouteInterface $main)
    {
        $this->initData();
        
        foreach(arrayGet($this->_data, 'routes', []) as $prefix => $conf) {
            $main->addRoute($prefix, function($request) use ($conf) {
                $instance = getInstance(
                    arrayGet($conf,['manager']),   // classe da instanzisare
                    $this,
                    arrayGet($conf,['conf']),         // stringa di configurazione
                    arrayGet($conf,['init'])    // parametri di implementazione
                );
                $instanceId = $conf['instance'];
                $instanceRec = arrayGet($this->_data, ['instance', $instanceId]);
                $instance->setDataSource('', new DataSource(arrayGet($instanceRec, 'conf')));
                $instance->setDataSource('main', $instance->getDataSource());
                return $instance->run($request);
            });
        }
    }

    public function install(iterable $instanceList)
    {
        $data = ['routes' => [], 'instance' => []];
        foreach($instanceList as $instance) {
            $managers = arrayGetAssert($instance, 'managerList', 'Nessun manager impostato');
            list($ice, $ds) = $this->getIce($instance);
            $data['instance'][$ice['id']] = $ice;
            $anagModel = null;
            foreach($this->getModels(fspath(__DIR__, 'MainInteractorDataSource','ModelInstance'), $ds) as $model) {
                $this->getConsole()->log('Create Model : '.$model->getResource());
                $model->init();
                if ($model->getResource()=='osy_ang') {
                    $anagModel = $model;
                }
            }
            foreach($managers as $manager) {
                $admin = $manager['admin'];
                $manager['instance'] = $ice['id'];
                $manager['id'] = (string) $ds->getCounter()->next();
                $data['routes'][$manager['prefix']] = $manager;
                if ($admin) {
                    $anag = [
                        'id_url' => $manager['id'],
                        'nme'    => $admin['name'],
                        'aut_lgn'=> $admin['name'],
                        'aut_pwd'=> $admin['passwd'],
                        'aut_act'=> '1',
                    ];
                    if ($anagModel) $anagModel->set($anag)->save();
                }
            }
        }
        $this->_fs->write($this->_fname, json_encode($data, JSON_PRETTY_PRINT));
        $this->initData();
    }

    private function getIce($instance) {
        $name = arrayGet($instance, 'name');
        $home = arrayGet($instance, ['info', 'home'], $name);
        $datasource = arrayGetAssert($instance, 'datasource', 'DataSource non impostato');

        $ice = ['conf'=>$datasource, 'name'=>$name, 'home'=>$home, 'id' => randHex(5)];
        $this->getConsole()->log('Create DataSource : '.$name);
        $ds = new DataSource($datasource);
        $ds->setCounter(new Maker($ice['id']));
        return [$ice, $ds];
    }

}