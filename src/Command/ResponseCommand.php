<?php
namespace Spinit\Dev\AppRouter\Command;

use Spinit\Dev\AppRouter\Core\HasResponseTrait;
use Spinit\Dev\AppRouter\Response;

abstract class ResponseCommand {

    use HasResponseTrait;

    public function __construct(Response $response) {

        $this->setResponse($response);
    }

    abstract public function exec();
}