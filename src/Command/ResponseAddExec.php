<?php
namespace Spinit\Dev\AppRouter\Command;

class ResponseAddExec extends ResponseCommand {
    public function exec() {
        $this->getResponse()->add('data,exec', func_get_arg(0));
    }
}