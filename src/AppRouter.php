<?php

declare(strict_types=1);

namespace Spinit\Dev\AppRouter;

use Webmozart\Assert\InvalidArgumentException;
use Spinit\Dev\AppRouter\Core\RouteNotFoundException;
use Spinit\Dev\Opensymap\Exception\MessageError;


use function Spinit\Util\arrayGet;

/**
 * AppRouter
 */
class AppRouter
{
    private $_list = [];
    public function addRoute($prefix, $executor) : void
    {
        $prefix = trim($prefix, '/');
        $this->_list[$prefix]  = $executor;
    }

    public function isEmpty() {
        return count($this->_list) == 0;
    }
    
    public function run(Request $request)
    {
        $found = false;
        $uri = ltrim($request->getPath().'/', '/'); 
        $base = $request->getBasePath();
        foreach ($this->_list as $prefix => $executor) {
            $prefix2 = ltrim($prefix.'/', '/');
            // se è presente un prefix vuoto e non è stato ancora impostato nulla .. viene usato
            if (!$prefix and !$found) {
                $found = [$prefix2, $executor];
            }
            if (substr($uri, 0, strlen($prefix2)) == $prefix2 and strlen($prefix) > strlen((string) arrayGet($found, 0))) {
                $found = [$prefix2, $executor];
            }
        }
        if (!$found) {
            throw new RouteNotFoundException();
        }
        try {
            if ($base) $base.='/';
            $request->setPath([$base.$found[0], substr($uri, strlen($found[0]))]);
            return call_user_func($found[1], $request, $found[0]);
        } catch (InvalidArgumentException $ex) {
            throw new MessageError($ex->getMessage(), "Attenzione");
        }
    }
}
