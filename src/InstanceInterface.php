<?php

declare(strict_types=1);

namespace Spinit\Dev\AppRouter;

use Spinit\Util\TriggerInterface;

interface InstanceInterface extends TriggerInterface {
    public function run(Request $request);
    public function getDataSource($name = '');
    public function getDataSourceList() : iterable;
    public function getResponse();
    public function getRequest();
    public function setUser($user);
    public function getUser();    
    public function hasUser();
    public function getConfiguration();
    public function getFormatter(string $name) : FormatterInterface;
}