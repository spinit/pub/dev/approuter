<?php
namespace Spinit\Dev\AppRouter\Helper;

use Spinit\Dev\AppRouter\Core\Console;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;

use function Spinit\Util\getenv;

class ConsoleWamp extends Console {
    private $main;
    public function __construct(MainInteractorInterface $main) {
        $this->main = $main;
    }
    public function log()
    {
        $this->main->publish('console.log', func_get_args());
        $this->wait();
    }
    public function error()
    {
        $this->main->publish('console.error', func_get_args());
        $this->wait();
    }
    public function info() {
        $this->main->publish('console.info', func_get_args());
    }
    private function wait() {
        $delay = (int) getenv('CONSOLE_DELAY');
        if ($delay) sleep($delay);
    }
}