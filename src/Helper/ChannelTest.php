<?php

namespace Spinit\Dev\AppRouter\Helper;

use Spinit\Dev\AppRouter\Helper\ChannelInterface;

use function Spinit\Dev\AppRouter\debug;

class ChannelTest implements ChannelInterface {
    
    public function header($code, $headers)
    {
        echo "HTTP ".$code."\n";
        foreach($headers as $name=>$list) {
            foreach($list as $value) {
                echo "{$name}: {$value}\n";
            }
        }
        echo "\n";
    }
    public function write($content)
    {
        echo $content;
    }

    public function open() {
        ob_start();
    }

    public function close()
    {
        return ob_get_clean();
    }
}