<?php

namespace Spinit\Dev\AppRouter\Helper;

interface ChannelInterface {
    public function header($code, $headerList);
    public function write($content);
    public function open();
    public function close();
}