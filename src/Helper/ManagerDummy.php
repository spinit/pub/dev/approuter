<?php

namespace Spinit\Dev\AppRouter\Helper;

use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\InstanceManager;
use Spinit\Dev\AppRouter\Request;
use Spinit\Lib\DataSource\DataSourceInterface;

use function Spinit\Dev\AppRouter\debug;

class ManagerDummy extends InstanceManager {
    
    public function run(Request $request, ?MainInteractorInterface $mi = null)
    {
        $ds = $this->getDataSource();
        $admin = $ds->query('SELECT * FROM osy_ang')->first();
        return 'message : '.$this->getInit('message').
               "\nconf : ".$this->getConf().
               "\nadmin : {$admin['aut_lgn']} {$admin['aut_pwd']}";
    }

    public function install() {
        $this->getConsole()->log('Dummy :: install 1');
        sleep(5);
        $this->getConsole()->log('Dummy :: install 2');
    }
}