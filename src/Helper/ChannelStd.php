<?php

namespace Spinit\Dev\AppRouter\Helper;

use Spinit\Dev\AppRouter\Helper\ChannelInterface;

use function Spinit\Dev\AppRouter\debug;

/**
 * @codeCoverageIgnore
 */
class ChannelStd implements ChannelInterface {

    public function header($code, $headerList)
    {
        http_response_code($code);
        foreach($headerList as $name=>$list) {
            $this->sendHeader($name, $list);
        }           
    }

    private function sendHeader($name, $list) {
        if (!is_array($list)) $list = [$list];
        foreach($list as $value) {
            header("{$name}: {$value}\n");
        }
    }
    public function write($content)
    {
        $this->sendHeader('Content-Length', mb_strlen($content));
        $this->sendHeader('Connection', 'close');
        echo $content;
    }

    public function open() {
        ob_start();
    }

    public function close()
    {
        while(ob_get_level()) {
            ob_end_flush();  
        } 
        @ob_flush();
        flush();
        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }
    }
}