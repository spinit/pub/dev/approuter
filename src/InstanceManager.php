<?php

declare(strict_types=1);

namespace Spinit\Dev\AppRouter;

use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Lib\DataSource\DataSourceInterface;
use Spinit\UUIDO\Maker;
use Webmozart\Assert\Assert;

use function Spinit\Util\arrayGet;

abstract class InstanceManager  extends Instance implements InstanceManagerInterface {

    private $conf;
    private $init;
    private $info;
    private $counter;

    /**
     * MainInteractorInterface
     */
    private $mi;

    public function __construct(MainInteractorInterface $mi = null, $conf = [], $init = [], $info=[])
    {
        $this->setMainInteractor($mi);
        $this->conf = $this->_makeConf($conf);
        $this->init = $this->_makeInit($init);
        $this->info = $info;
        $this->counter = new Maker($this->getInfo('id_ice'));
        parent::__construct();
    }

    public function getInfo($name = '') {
        if ($name) {
            return arrayGet($this->info, $name);
        } 
        return $this->info;
    }

    public function getDataSource($name = '') {
        $ds = parent::getDataSource($name);
        $ds->setCounter($this->counter);
        try {
            $ds->getAdapter()->setParam('userTrace', $this->getUser('id'));
        } catch (\Exception $e) {
            
        }
        return $ds;
    }

    public function setMainInteractor(MainInteractorInterface $main = null) {
        $this->mi = $main;
        return $this;
    }
    public function getMainInteractor(): MainInteractorInterface {
        Assert::notNull($this->mi);
        return $this->mi;
    }

    public function getConsole() {
        return $this->getMainInteractor()->getConsole();
    }
    protected function _makeConf($conf) {
        return $conf;
    }
    public function getConf() {
        return $this->conf;
    }
    
    protected function _makeInit($init) {
        if (is_string($init)) $init = json_decode($init, true)?:[];
        return new Data($init);
    }
    public function getInit() {
        return call_user_func_array([$this->init, 'get'], func_get_args());
    }

}