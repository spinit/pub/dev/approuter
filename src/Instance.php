<?php

declare(strict_types=1);

namespace Spinit\Dev\AppRouter;

use Spinit\Dev\AppRouter\Core\HasDataSourceTrait;
use Spinit\Dev\AppRouter\Core\HasRequestTrait;
use Spinit\Dev\AppRouter\Core\HasResponseTrait;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Util\Error\MessageException;
use Spinit\Util\Error\NotFoundException;
use Spinit\Util\TriggerTrait;
use Webmozart\Assert\Assert;

use function Spinit\Util\arrayGet;
use function Spinit\Util\getenv;

abstract class Instance  implements InstanceInterface {

    use HasDataSourceTrait;
    use HasResponseTrait;
    use HasRequestTrait;
    use TriggerTrait;

    private $_basePath = '';
    private $_user;
    private $_data;
    private $_userCached = false;

    public function __construct()
    {
        $this->bindExec('flush', function() {
            foreach($this->getDataSourceList() as $ds) {
                $ds->getAdapter()->trigger('flush');
            }
        });        
        $this->init();
    }
    
    protected function init() { }
    
    abstract public function getMainInteractor() : MainInteractorInterface;

    public function getConfiguration() {
        $this->_data = $this->_data ?: new Data();
        $args = func_get_args();
        if (count($args)==0) return $this->_data;
        return call_user_func_array([$this->_data, 'get'], $args);
    }

    public function getInfo($name = '') {}

    public function setBasePath($base) {
        $this->_basePath = trim($base, '/');
        return $this;
    }
    public function getHome() {
        $path = call_user_func_array('Spinit\Dev\AppRouter\fspath', func_get_args());
        $dir = $this->getMainInteractor()->getHome(fspath($this->getInfo('home'), $path));
        if (!is_dir($dir)) {
            if (!@mkdir($dir, 0777, true)) {
                throw new MessageException("Errore creazione directory : ".$dir);
            }
        }
        return $dir;
    }
    public function makePath($path, $withScheme = false) {
        Assert::notEmpty($this->_basePath, 'BasePath non impostato');
        $url = '//'.implode('/',[
            $this->_basePath, 
            ltrim($path.'','/')
        ]);
        if ($withScheme) {
            $url = $this->getRequest()->getScheme().':'.$url;
        }
        return $url;
    }

    public function getUser() {
        if ($this->_user === null) {
            $user = $this->getMainInteractor()->getUser($this->getRequest());
            if (!$user) {
                throw new NotFoundException("Accesso non effetuato");
            }
            Assert::isArray($user);
            $this->_user = new Data($user);
        }
        if ($this->_user and $this->_user->get("id_url") != $this->getInfo("id")) $this->setUser(null);
        
        $args = func_get_args();
        
        return count($args) ? call_user_func_array([$this->_user, 'get'], $args) : $this->_user;
    }
    
    public function setUser($user) {

        $path = explode('/', ltrim($this->_basePath, '/'));
        $path[0] = ''; // viene tolto il dominio;
        $path = rtrim(implode('/', $path), '/') ?: '/';

        if (!$user or @$user["id_url"] != $this->getInfo("id")) {
            $this->_user = null;
            $this->getMainInteractor()->setUser($this->getRequest(), '', $path);
        } else {
            if (is_array($user)) {
                $this->_user = new Data($user);
            } else if ($user instanceof Data) {
                $this->_user = $user;
            } else {
                $args = func_get_args();
                Assert::count($args, 2, 'Chiamata errata metodo '.__METHOD__);
                Assert::notNull($this->_user);
                call_user_func_array([$this->_user, 'set'], $args);
            }
            $data = $this->_user->get('');
            $this->getMainInteractor()->setUser($this->getRequest(), $data, $path);
        }
        return $this;
    }

    public function hasUser() {
        if (!$this->_user and !$this->_userCached) {
            $this->_userCached = true;
            try {
                $this->getUser();
            } catch (\Exception $e) {}
        }
        // controllo su provenienza dell'autenticazione
        if(@$this->_user["id_url"] != $this->getInfo("id")) {
            $this->setUser(null);
        }
        return $this->_user !== null;
    }

    public function normalizeQuery($query) {
        // ricerca del pattern @DataSource._@ per poter utilizzare risorse di database differenti
        if (preg_match_all("/\@DataSource\.([^@]*)\@/", $query, $matches)) {
            foreach($matches[0] as $k=>$v) {
                $nDS = $this->getDataSource($matches[1][$k]);
                $query = str_replace($v, $nDS->getAdapter()->getName(), $query);
            }
        }
        return $query;
    }

    private $formatter = [];
    public function setFormatter(string $name, FormatterInterface $formatter) {
        $this->formatter[$name] = $formatter;
        return $this;
    }
    public function getFormatter(string $name) : FormatterInterface {
        return arrayGet($this->formatter, $name) ? : new FormatterIdentity();
    }
    public function publish($topic, $param = [], $realm = 'realm1') {
        return $this->getMainInteractor()->publish($this->getInfo("id").":".$topic, $param, $realm);
    }
}
