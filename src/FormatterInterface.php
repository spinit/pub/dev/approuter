<?php
namespace Spinit\Dev\AppRouter;

interface FormatterInterface {
    function format($value) : string;
}