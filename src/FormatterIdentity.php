<?php
namespace Spinit\Dev\AppRouter;

class FormatterIdentity implements FormatterInterface {
    function format($value): string
    {
        return (string) $value;
    }
}