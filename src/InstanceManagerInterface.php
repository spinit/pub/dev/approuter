<?php

declare(strict_types=1);

namespace Spinit\Dev\AppRouter;

use Spinit\Dev\AppRouter\Core\MainInteractorInterface;

interface InstanceManagerInterface extends InstanceInterface {
    function install();
    function getConf();
    function getInit();
    function getMainInteractor(): MainInteractorInterface;
    function getConsole();
}
