<?php
namespace Spinit\Dev\AppRouter;

use function Spinit\Util\nvl;

use React\Socket\Connection as SocketConnection;
use Thruway\ClientSession;
use Thruway\Connection;
use Webmozart\Assert\Assert;

class WampService {

    private $_server;
    private $_realm;

    public function __construct($server, $realm = 'realm1')
    {
        Assert::notEmpty($server, "WAMP : Server non impostato");
        Assert::notEmpty($realm, "WAMP : realm non impostato");

        $this->_server = $server;
        $this->_realm = $realm;    
    }

    public function publish($topic, $param = [], $realm = '') {
    
        if (!$realm) $realm = nvl($this->_realm, 'realm1');

        $par = [
            "realm"   => $realm,
            "url"     => 'ws://'.$this->_server,
        ];
        if (!is_array($param)) {
            $param = [$param];
        }
        ob_start();
        $connection = new Connection($par);

        $connection->on(
            'open',
            function (ClientSession $session) use ($connection, $topic, $param) {
                $session->publish($topic, $param, [], ["acknowledge" => true])->then(
                    function () use ($connection) {
                        $connection->close(); //You must close the connection or this will hang
                        echo "Publish Acknowledged!\n";
                    },
                    function ($error) {
                        // publish failed
                        echo "Publish Error {$error}\n";
                    }
                );
            }
        );
        $connection->open();
        return ob_get_clean();
    }

}