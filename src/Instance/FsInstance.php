<?php


namespace Spinit\Dev\AppRouter\Instance;

use Spinit\Dev\AppRouter\Core\Asset;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Instance;
use Spinit\Dev\AppRouter\Request;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;

class FsInstance extends Instance {

    private $_rootView;
    private $_mi;

    public function __construct(string $rootView, MainInteractorInterface $interactor) {
        parent::__construct();
        $this->_rootView = $rootView;
        $this->_mi = $interactor; 
    }

    public function getMainInteractor() : MainInteractorInterface{
        Assert::notNull($this->_mi);
        return $this->_mi;
    }

    public function run (Request $request) {
        $this->setRequest($request);
        $asset = new Asset($this, $this->_rootView);
        $result = $asset->run($request->getPath(), (string) $request->getApp('osy,event'), $request);
        $response = $this->getResponse();
        if (is_string($result)) {
            $response->setContent(function() use ($result) {
                return $result;
            });
        } else if (is_array($result)) {
            $response->set($result);
        }
        return $response;
    }

    /**
     * Si prova a generare il path con la versione del contenuto
     */
    public function makePath($path, $withScheme = false)
    {
        $fname = fspath($this->_rootView, $path);
        $result = parent::makePath($path, $withScheme);
        if (is_file($fname)) {
            $result.='?_sha='.sha1_file($fname);
        }
        return $result;
    }
}