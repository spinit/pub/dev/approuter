<?php

namespace Spinit\Dev\AppRouter;

use ArrayObject;
use JsonSerializable;
use Webmozart\Assert\Assert;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

class Data extends ArrayObject implements JsonSerializable {


    public function jsonSerialize() {
        return (array) $this;
    }

    public function clear() {
        foreach($this->getIterator() as $k => $v) $this->offsetUnset($k);
        return $this;
    }
    public function set($data) {
        $args = func_get_args();
        if (count($args)<2) {
            Assert::isArray($data);
            foreach($data as $k=>$v) $this[$k] = $v;
        } else {
            $value = $args[1];
            $data = asArray($data, ',');
            if (count($data) == 0) {
                // la chiave è vuota => sovrascrittura ... $value deve essere un array
                $this->clear()->set($value);
            } else {
                // $data contiene il percorso in cui memorizzare $value
                $d = &$this;
                foreach($data as $n) {
                    if ($d instanceof self OR $d instanceof \ArrayObject) {
                        if (!$d->offsetExists($n)) $d[$n] = '';
                    } else {
                        if (!is_array($d))  $d = [];
                        if (!array_key_exists($n, $d)) $d[$n] = '';
                    }
                    $d = &$d[$n];
                }
                $d = $value;
                if ($d !== $this) unset($d);
            }
        }
        return $this;
    }

    public function get($names='', $default = '') {
        $keys = asArray($names, ',');
        if (count($keys)==0) {
            return (array) $this;
        }
        return arrayGet($this, $keys, $default, false);
    }
    public function has($name) {
        $name = asArray($name, ',');
        $d = &$this;
        foreach($name as $n) {
            if ($d instanceof self OR $d instanceof \ArrayObject) {
                if (!$d->offsetExists($n)) return false;
            } else {
                if (!array_key_exists($n, $d?:[])) return false;
            }
            $d = &$d[$n];
        }
        return true;
    }
    
    public function add($name, $data) {
        $name = asArray($name, ',');
        $d = &$this;
        foreach($name as $n) {
            if ($d instanceof self OR $d instanceof \ArrayObject) {
                if (!$d->offsetExists($n)) $d[$n] = [];
            } else {
                if (!array_key_exists($n, $d)) $d[$n] = [];
            }
            if (!is_array($d[$n]) AND  !($d[$n] instanceof self) AND  !($d[$n] instanceof \ArrayObject)) {
                $d[$n] = [$d[$n]];
            }
            $d = &$d[$n];
        }
        $d[] = $data;
        return $this;
    }

}
