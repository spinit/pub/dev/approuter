<?php
namespace Spinit\Dev\AppRouter;

require_once __DIR__. '/../autoload.php';

use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Spinit\Dev\AppRouter\Entity\MainInteractorJson;
use Spinit\Dev\AppRouter\Entity\MainStandard;
use Spinit\Dev\AppRouter\Helper\ManagerDummy;

use function Spinit\Dev\AppRouter\debug;

header('Content-Type: text/plain; charset=utf-8');

// Dove poter controllare la configurazione
$fs = new Filesystem(new LocalFilesystemAdapter(dirname(__DIR__).'/.cache/web'));
// chi gestisce le operazioni principali?
$interactor = new MainInteractorJson('xroutes.json', $fs);

$interactor->setHome(dirname(__DIR__).'/var/home');

// processo principale
$app = new MainStandard($interactor);

Config::addManager(ManagerDummy::class, ['label'=>'Manager di prova']);

Config::addInstance('main', [
    'manager'=>ManagerDummy::class, 
    'label'=>'Main',
    'slug' => 'prova',
    'admin'=> true,
    'init' => ['message'=>'Manager di sviluppo']
]);

// avvio processo
echo $app->run();