<?php

namespace Spinit\Test\Unit\Core;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Core\Asset;
use Spinit\Dev\AppRouter\Helper\ChannelInterface;
use Spinit\Test\Src\TestInstance;

use function Spinit\Dev\AppRouter\debug;

class AssetTest extends TestCase implements ChannelInterface
{
    /**
     * @var Asset
     */
    private $asset;
    private $chain;

    protected function setUp(): void
    {
        parent::setUp();
        $this->asset = new Asset(new TestInstance(), __DIR__);
    }

    public function open() {
        $this->chain = new \stdClass;
    }

    public function header($code, $headerList) {
    }
    
    public function write($content){
        $this->chain->content = $content;
    }

    public function close(){}

    public function testTextFile() {
        $path = $this->asset->makePath('test/file.txt');
        $this->assertStringContainsString('?_sha=', $path);
        $response = $this->asset->run('test/file.txt');
        ob_start();
        $response->send($this);
        $content = ob_get_clean();
        $this->assertStringContainsString('this is a test file', $content);
    }
    
    public function testPhpFile() {
        
        $response = $this->asset->run('test/file.php');
        $this->assertEquals([], json_decode($response->getContent(), true));
        $response = $this->asset->run('test/file');
        $this->assertEquals([], json_decode($response->getContent(), true));
    }
    public function testPhpFile2() {
        $response = $this->asset->run('test/file/1/hello');
        $this->assertEquals(['1','hello'], json_decode($response->getContent(), true));
    }
    public function testPhpFile3() {

        $response = $this->asset->run('test/file/1/hello','event');
        $this->assertEquals(['1','hello','OK'], json_decode($response->getContent(), true));
    }
    
}