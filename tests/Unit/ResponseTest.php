<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\AppRouter\Helper\ChannelInterface;
use Spinit\Dev\AppRouter\ResponseException;

use function Spinit\Dev\AppRouter\debug;

class ResponseTest  extends TestCase implements ChannelInterface {

    /**
     * @var Response
     */
    private $response;
    private $chain;

    protected function setUp() : void
    {
        parent::setUp();
        $this->response = new Response();
    }
    
    public function open() {
        $this->chain = new \stdClass;
    }

    public function header($code, $headerList) {
        $this->chain->code = $code;
        $this->chain->headers = $headerList;
    }
    public function write($content){}
    public function close(){}

    public function testResponse() {
        $this->response->setStatusCode(200);
        $this->assertEquals(200, $this->response->getStatusCode());

        $this->response->setContent(function() {
            return 'ciao';
        },'text/test');
        
        $this->response->then(function() {
            $this->chain->then = 'ok';
        });
        $this->response->send($this);
        $this->assertEquals(200, $this->chain->code);
        $this->assertEquals(['text/test'], $this->chain->headers['Content-Type']);
        $this->assertEquals('text/test', $this->response->getHeader('Content-Type'));
        $this->assertEquals('ok', $this->chain->then);
        
    }

    public function testDataManipulation()
    {
        $this->response->set(['uno'=>'due']);
        $this->assertEquals(['uno'=>'due'], json_decode($this->response->getContent(), true));
        $this->response->set(['tre'=>4]);
        $this->assertEquals(['uno'=>'due', 'tre'=>4], json_decode($this->response->getContent(), true));
        $this->response->set('', ['tre'=>4]);
        $this->assertEquals(['tre'=>4], json_decode($this->response->getContent(), true));
        $this->response->set('tre,ok', ['1'=>1]);
        $this->assertEquals(['tre'=>['ok'=>['1'=>1]]], json_decode($this->response->getContent(), true));

        $this->response->add('tre,uno', 1);
        $this->assertEquals(['tre'=>['ok'=>['1'=>1],'uno'=>[1]]], json_decode($this->response->getContent(), true));

        $this->assertEquals([1], $this->response->get('tre,uno'));

        $this->response->set('tre,uno', 1);
        $this->assertEquals(1, $this->response->get('tre,uno'));

        $this->assertEquals(1, $this->response->get('tre,ok,1'));

    }

    public function testStop() {
        $this->response->setContent(function(){
            $this->chain->stop = true;
            $this->response->stop();
        });
        $this->response->send($this);
        $this->assertTrue($this->chain->stop);
    }
}