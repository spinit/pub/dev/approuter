<?php

namespace Spinit\Test\Unit\Entity;

use ArrayObject;
use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Entity\InstanceInstall\InteractorInterface;
use Spinit\Dev\AppRouter\Entity\MainRouter;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\ResponseException;
use Spinit\Test\Src\TestChannel;

use function Spinit\Dev\AppRouter\debug;

class MainRouterTest extends TestCase implements MainInteractorInterface
{
    /**
     * @var MainRouter
     */
    private $main;

    protected function setUp() : void
    {
        parent::setUp();
        $this->main = new MainRouter($this);
        $this->assertEquals($this, $this->main->getInteractor());
    }

    public function install(iterable $instanceList)
    {
        
    }
    public function initRoute(AddRouteInterface $main)
    {
        
    }
    public function publish(string $topic, array $param, $realm = '')
    {
        
    }

    public function getConsole()
    {
        
    }
    public function getHome() {

    }
    public function encrypt($data)
    {
        
    }
    public function decrypt($token)
    {
        
    }
    public function getUser(?Request $request)
    {
        
    }
    public function setUser(?Request $request, $user, $path)
    {
        
    }
    public function searchUser($id_url, $name, $passwd, $ds = null) {
        return null;
    }
    /**
     * @xcovers
     */
    public function testInstaller() {
        $channel = new TestChannel();
        $response = $this->main->run(new Request('GET', 'localhost:8080'));
        $response->send($channel);
        $result = $channel->getContent();
        $this->assertStringContainsString('form', $result, 'result => '.$result);
        $this->assertStringNotContainsString('app-event="install"', $result);

        $response = $this->main->run(new Request('GET', 'localhost:8080/style.css'));
        $response->send($channel);
        $this->assertStringContainsString('.main', $channel->getContent());
    }

    public function testStyleCss() {
        $channel = new TestChannel();
        $response = $this->main->run(new Request('GET', 'localhost:8080/style.css'));
        $response->send($channel);
        $this->assertStringContainsString('.main', $channel->getContent());
    }

    /**
     * @xcovers
     */
    public function testNotFoundInstance() {
        $this->main->addRoute('qualsiasi', function() {});
        $result = $this->main->run(new Request('GET', 'non.configurato/ciao'));
        $channel = new TestChannel();
        $result->send($channel);
        $this->assertStringContainsString('osy-instance="not-found"', $channel->getContent());
    }
    
    /**
     * @xcovers
     */
    public function testFoundInstance() {
        $this->main->addRoute('instance.found/uno', function() {
            return 'Found';
        });
        $channel = new TestChannel();
        $result = $this->main->run(new Request('GET', 'instance.found/uno'));
        $this->assertEquals('Found', $result);
        $result = $this->main->run(new Request('GET', 'instance.found/unoo'));
        $result->send($channel);
        $this->assertStringContainsString('osy-instance="not-found"', $channel->getContent());
    }
    
}
