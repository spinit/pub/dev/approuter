<?php

namespace Spinit\Test\Unit\Entity;

use League\Flysystem\Filesystem;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Entity\MainInteractorJson;
use Spinit\Test\Src\TestInstanceInstallInteractor;

use function Spinit\Dev\AppRouter\debug;

class MainInteractorJsonTest extends TestCase implements AddRouteInterface
{
    private $_routes = [];
    private $_mainContent = <<< EOINI
    {
        "routes" : {
            "prima/route" : {
                "class" : "Spinit:Test:Unit:Entity:MainInteractorIniter"
            },
            "seconda.route" : {
                "class" : "Spinit:Test:Unit:Entity:IniterNotFound"
            }
        }
    }
    EOINI;

    public function addRoute($route, $inter)
    {
        $this->_routes[$route] = $inter;
    }

    /**
     * @xcovers
     */
    public function testFoundFile() {
        $fs = new Filesystem(new InMemoryFilesystemAdapter());
        $fs->write('MainInteractoJsonRoutes.json', $this->_mainContent);
        
        $object = new MainInteractorJson('MainInteractoJsonRoutes.json', $fs);
        $this->_routes = [];
        $object->initRoute($this);
        $this->assertEquals(['prima/route', 'seconda.route'], array_keys($this->_routes));
    }
    
    /**
     * @xcovers
     */
    public function testInstall() {
        $fs = new Filesystem(new InMemoryFilesystemAdapter());
        $this->assertFalse($fs->fileExists('MainInteractoJsonRoutes.json'));

        $object = new MainInteractorJson('MainInteractoJsonRoutes.json', $fs);
        $instanceList = [
            [
                'name'=>'main',
                'datasource'=>'sqlite:memory',
                'managerList' => [
                    [
                        'prefix' => 'dopo.uno/viene/due', 
                        'manager'  => __CLASS__,
                        'admin' => ['name' => 'test', 'passwd' => 'pwd']
                    ]
                ]
            ]
        ];

        $object->install($instanceList);
        $this->assertTrue($fs->fileExists('MainInteractoJsonRoutes.json'));

        $this->_routes = [];
        $object->initRoute($this);
        $this->assertEquals(['dopo.uno/viene/due'], array_keys($this->_routes));
   }
    
}