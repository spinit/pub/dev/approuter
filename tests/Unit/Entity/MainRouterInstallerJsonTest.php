<?php

namespace Spinit\Test\Unit\Entity;

use ArrayObject;
use League\Flysystem\Filesystem;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Config;
use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Entity\MainInteractorJson;
use Spinit\Dev\AppRouter\Entity\MainRouter;
use Spinit\Dev\AppRouter\Request;
use Spinit\Lib\DataSource\DataSource;

use function Spinit\Dev\AppRouter\debug;

class MainRouterInstallerJsonTest extends TestCase
{
    /**
     * @var MainRouter
     */
    private $_app;

    protected function setUp() : void
    {
        parent::setUp();
        $ds = new DataSource('sqlite::memory');
        if($ds->check('osy_ang')) $ds->exec('delete from osy_ang');
        // Dove poter controllare la configurazione
        $fs = new Filesystem(new InMemoryFilesystemAdapter());
        // chi gestisce le operazioni principali?
        $this->assertFalse($fs->fileExists('routes.json'));
        $interactor = new MainInteractorJson('routes.json', $fs);
        $app = new MainRouter($interactor);
        if(Config::getInstanceList()) Config::getInstanceList()->clear();
        Config::addInstance('main', [
            'manager'=> 'Spinit:Dev:AppRouter:Helper:ManagerDummy', 
            'init'=>['message'=>self::class],
            'slug'=>'prova',
            'admin'=>true
        ]);
        $this->_app = $app;
    }
    
    public function testInstallError() {
        $request = new Request('POST', 'localhost');
        $request->setApp('osy,event', 'install');
        
        $response = $this->_app->run($request);
        $this->assertEquals('error', $response->get('status'));        
    }

    public function testInstallOk() {
        $request = new Request('POST', 'localhost');
        
        $request->setApp('osy,event', 'install');
        $request->setPost([
            'txt_nme'=>['main'=>'test1'], 
            'txt_pwd'=>['main'=>'test2'], 
            'txt_cns' => ['main'=>'sqlite::memory'],
            'txt_path'=>['main'=>'localhost/test'], 
            'txt_conf'=>['main'=>'test:prova']
        ]);
        $response = $this->_app->run($request);
        $this->assertEquals('success', $response->get('status'), $response->get('result').'');  
        $this->assertEquals('cmd', $response->get('data,exec,0,0'));  

        $response = (string) $this->_app->run(
            new Request('GET', 'localhost/test')
        );
        $this->assertStringContainsString(self::class, $response);
        $this->assertStringContainsString('admin : test1 '.md5('test2'), $response);
        $this->assertStringContainsString('conf : test:prova', $response);
    }
}
