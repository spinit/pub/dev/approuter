<?php

namespace Spinit\Test\Unit\Entity;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Config;
use Spinit\Dev\AppRouter\Entity\MainInteractorDataSource\Interactor as MainInteractorDataSource;
use Spinit\Dev\AppRouter\Entity\MainRouter;
use Spinit\Dev\AppRouter\Request;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\Src\TestChannel;
use Spinit\Test\Src\TestInstanceInstallInteractor;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getenv;

class MainRouterInstallerDataSourceTest extends TestCase
{
    /**
     * @var MainRouter
     */
    private $_app;

    protected function setUp() : void
    {
        parent::setUp();
        $ds = new DataSource('sqlite::memory');
        if($ds->check('osx_ice')) $ds->exec('delete from osx_ice');
        if($ds->check('osy_ice_url')) $ds->exec('delete from osx_ice_url');
        if($ds->check('osy_ang')) $ds->exec('delete from osy_ang');
        $interactor = new MainInteractorDataSource($ds);
        $app = new MainRouter($interactor);
        if(Config::getInstanceList()) Config::getInstanceList()->clear();
        Config::addInstance('main', [
            'manager'=> 'Spinit:Dev:AppRouter:Helper:ManagerDummy', 
            'init'=>['message'=>self::class],
            'slug'=>'prova',
            'admin'=>true
        ]);
        $this->_app = $app;
    }
    
    public function testInstallError() {
        $request = new Request('POST', 'localhost');
        $request->setApp('osy,event', 'install');
        $response = $this->_app->run($request);
        $this->assertEquals('error', $response->get('status'));        
    }

    public function testInstallOk() {
        //getenv('STOP', 1);
        $request = new Request('POST', 'localhost');
        $request->setApp('osy,event', 'install');
        $request->setPost([
            'txt_nme'=>['main'=>'test1'], 
            'txt_pwd'=>['main'=>'test2'], 
            'txt_cns' => ['main'=>'sqlite::memory'],
            'txt_path'=>['main'=>'localhost/test'], 
            'txt_conf'=>['main'=>'test:prova']
        ]);
        $channel = new TestChannel();

        $response = $this->_app->run($request);
        $this->assertEquals('success', $response->get('status'), $response->get('result').'');  
        // invia un comando da eseguire
        $response->send($channel);
        $this->assertEquals('cmd', $response->get('data,exec,0,0'), $channel->getContent());  
        $ds = new DataSource('sqlite::memory');
        $this->assertFalse(!$ds->check('osx_ice'));
        $this->assertFalse(!$ds->check('osx_ice_url'));
        
        $url = $ds->query('select * from osx_ice_url')->first();
        $this->assertEquals(json_encode(['message'=>self::class]), $url['init']);
        $this->assertEquals('test:prova', $url['conf']);
        $response = $this->_app->run(new Request('GET', 'localhost/test'));
        if (is_string($response)) {
            $content = $response;
        } else {
            $response->send($channel);
            $content = $channel->getContent();
        }
        $this->assertStringContainsString(self::class, $content);
        $this->assertStringContainsString('admin : test1 '.md5('test2'), $response);
        $this->assertStringContainsString('conf : test:prova', $response);
    }
}
