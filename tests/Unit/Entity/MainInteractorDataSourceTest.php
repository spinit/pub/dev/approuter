<?php

namespace Spinit\Test\Unit\Entity;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Entity\MainInteractorDataSource\Interactor;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\Src\TestInstanceInstallInteractor;

use function Spinit\Dev\AppRouter\debug;

class MainInteractorDataSourceTest extends TestCase 
{
    private $_ds;

    public function getConnectionString()
    {
        return __CLASS__;
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->_ds = $ds = new DataSource('sqlite::memory');
        if($ds->check('osx_ice')) $ds->exec('delete from osx_ice');
        if($ds->check('osx_ice_url')) $ds->exec('delete from osx_ice_url');
    }

    /**
     * @xcovers
     */
    public function testInstall() {
        // database main
        $mi = new Interactor($this->_ds);
        $instanceList = [];
        // database istanza
        $mi->install($instanceList);
        $this->assertEquals('osx_ice', $this->_ds->check('osx_ice')['name']);
        $this->assertEquals('osx_ice_url', $this->_ds->check('osx_ice_url')['name']);
    }
    
}