<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Config;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;

class ConfigTest extends TestCase {

    public function testManagers() {
        $data = ['qualsiasi'=>'cosa', 'un'=>['dato'=>['a'=>'caso']]];
        Config::addManager('Un:nome', $data);
        $this->assertEquals($data, (array) Config::getManagerList()->get('Un:nome'));
        $this->assertEquals('caso', Config::getManagerList()->get('Un:nome,un,dato,a'));
        try {
            Config::getManager('non:esiste');
            $this->fail('Exception not throw');
        } catch(NotFoundException $e) {}
        $this->assertEmpty(Config::getManagerList()->get('non:esiste'));
        $item = Config::getManager('Un:nome');
        $this->assertEquals('caso', $item->get('un,dato,a'));
        Config::getManagerList()->clear();
        $this->assertEmpty(Config::getManagerList()->get('Un:nome'));
    }

    public function testInstances() {
        $data = ['qualsiasi'=>'cosa', 'un'=>['dato'=>['a'=>'caso']]];
        Config::addInstance('Un:nome', $data);
        $this->assertEquals($data, (array)Config::getInstanceList()->get('Un:nome'));
        $item = Config::getInstance('Un:nome');
        $this->assertEquals('caso', $item->get('un,dato,a'));
        try {
            Config::getInstance('non:esiste');
            $this->fail('Exception not throw');
        } catch(NotFoundException $e) {}
        $this->assertEmpty(Config::getInstanceList()->get('non:esiste'));
        Config::getInstanceList()->clear();
        $this->assertEquals('', Config::getInstanceList()->get('Un:nome'));
    }
}