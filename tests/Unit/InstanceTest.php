<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Test\Src\TestInstance;

class InstanceTest extends TestCase {

    /**
     * @var Instance
     */
    private $obj;

    protected function setUp() : void
    {
        parent::setUp();
        $this->obj = new TestInstance();
    }

    public function testPath() {
        $this->obj->setBasePath('localhost:1111/osy');
        $this->assertEquals('//localhost:1111/osy/ok', $this->obj->makePath('ok'));
        $this->assertEquals('//localhost:1111/osy/ok', $this->obj->makePath('/ok'));
        $this->assertEquals('//localhost:1111/osy/ok/', $this->obj->makePath('ok/'));
        $this->assertEquals('//localhost:1111/osy/ok/', $this->obj->makePath('/ok/'));

        $this->obj->setBasePath('localhost:1111/osy/');
        $this->assertEquals('//localhost:1111/osy/ok', $this->obj->makePath('ok'));
        $this->assertEquals('//localhost:1111/osy/ok', $this->obj->makePath('/ok'));
        $this->assertEquals('//localhost:1111/osy/ok/', $this->obj->makePath('ok/'));
        $this->assertEquals('//localhost:1111/osy/ok/', $this->obj->makePath('/ok/'));
    }

    public function testConfiguration() {
        $this->obj->getConfiguration()->set('uno,due', 'tre');
        $this->assertEquals('tre', $this->obj->getConfiguration('uno,due'));
        $this->obj->getConfiguration()->set('default:value', [2,3]);
        $this->assertEquals([2, 3], $this->obj->getConfiguration('default:value'));
        $this->assertEquals(3, $this->obj->getConfiguration('default:value,1'));
        $ciccio = $this->obj->getFormatter('ciccio');
        $this->assertEquals('5', $ciccio->format(5));
    }
}