<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Data;

class DataTest extends TestCase {

    /**
     * @var Data
     */
    private $obj;

    protected function setUp() : void
    {
        parent::setUp();
        $this->obj = new Data();
    }
    

    /**
     * @xcovers
     */
    public function testDataManipulation()
    {
        $this->obj->set(['uno'=>'due']);
        $this->assertEquals(['uno'=>'due'], $this->obj->get());
        $this->obj->set(['tre'=>4]);
        $this->assertEquals(['uno'=>'due', 'tre'=>4], $this->obj->get());
        $this->obj->set('', ['tre'=>4]);
        $this->assertEquals(['tre'=>4], $this->obj->get());
        $this->obj->set('tre,ok', ['1'=>1]);
        $this->assertEquals(['tre'=>['ok'=>['1'=>1]]], $this->obj->get());

        $this->obj->add('tre,uno', 1);
        $this->assertEquals(['tre'=>['ok'=>['1'=>1],'uno'=>[1]]], $this->obj->get());

        $this->assertEquals([1], $this->obj->get('tre,uno'));

        $this->obj->set('tre,uno', 1);
        $this->assertEquals(1, $this->obj->get('tre,uno'));

        $this->assertEquals(1, $this->obj->get('tre,ok,1'));

        $this->obj->add('tre,due,uno', 'ok');
        $this->assertEquals(['ok'], $this->obj->get('tre,due,uno'));

        $this->assertTrue($this->obj->has('tre,due'));
        $this->assertTrue($this->obj->has('tre,due,uno'));
        $this->assertFalse($this->obj->has('tre,due,zero'));


    }

    /**
     * @xcovers
     */
    public function testIdentity() {
        $this->obj->set('tt', 1);
        $this->assertEquals(1, $this->obj->get('tt'));
        $this->obj->set('tt', []);
        $this->assertEquals([], $this->obj->get('tt'));

    }

    public function testSerialize() {
        $init = ['uno'=>'due', 'tre'=>['quattro'=>5]];
        $this->obj->set($init);
        $this->assertEquals(json_encode($init), json_encode($this->obj));
    }

    public function testIteration() {
        $data = ['uno'=>2, 'tre'=>[4,5]];
        $this->obj->set($data);
        foreach($this->obj as $k=>$v) {
            $this->assertEquals($data[$k], $v);
        }
    }

}