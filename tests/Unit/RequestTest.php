<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;

use function Spinit\Dev\AppRouter\debug;

class RequestTest extends TestCase {

    /**
     * @var Request
     */
    private $obj;
    
    protected function setUp() : void
    {
        parent::setUp();
        $this->obj = new Request('GET','una/prova');
    }
    
    public function testRequest() {
        $this->assertEquals('GET', $this->obj->getMethod());
        $this->assertEquals('http', $this->obj->getScheme());
        $this->assertEquals('una/prova', $this->obj->getPath());
        $this->assertEquals('una', $this->obj->getPath(0));
        $this->assertEquals('prova', $this->obj->getPath(1));
        $this->assertEquals('', $this->obj->getPath(2));
        $this->assertEquals(2, $this->obj->countPath());
    }

    public function testSchema() {
        $this->obj = new Request('GET','https://una/prova');
        $this->assertEquals('https', $this->obj->getScheme());
        $this->obj->setScheme('http');
        $this->assertEquals('http', $this->obj->getScheme());
    }

    public function testDataManipulation() {
        foreach(['Query', 'Post', 'Cookie', 'Files', 'App', 'Headers'] as $m) {
            $mget = 'get'.$m;
            $mset = 'set'.$m;
            $this->obj->$mset(['uno'=>'due']);
            $this->assertEquals(['uno'=>'due'], $this->obj->$mget(''));
            $this->assertEquals('due', $this->obj->$mget('uno'));
            $this->obj->$mset('uno,due', 'tre');
            $this->assertEquals('tre', $this->obj->$mget('uno,due'));
        }
    }

    public function testShiftPath() {
        $this->obj = new Request('GET','https://una/prova');
        $this->assertEquals('una', $this->obj->getPath(0));
        $this->assertEquals('', $this->obj->getBasePath());
        $req1 = $this->obj->shiftPath();
        $this->assertEquals('prova', $req1->getPath(0));
        $this->assertEquals('una', $req1->getBasePath());
        $req2 = $req1->shiftPath();
        $this->assertEquals('', $req2->getPath(0));
        $this->assertEquals('una/prova', $req2->getBasePath());
    }

    public function testGetUrl() {
        $this->assertEquals('http://una/prova', $this->obj->getUrl());
        $this->assertEquals('//una/prova', $this->obj->getUrl(false));
    }

    public function testDup() {
        $dup = $this->obj->dup();
        $this->assertEquals($this->obj->getMethod(), $dup->getMethod());
        $this->assertEquals($this->obj->getScheme(), $dup->getScheme());
        $this->assertEquals($this->obj->getPath(), $dup->getPath());
        $this->assertEquals($this->obj->getPath(0), $dup->getPath(0));
        $this->assertEquals($this->obj->getPath(1), $dup->getPath(1));

        $this->obj->setQuery('uno', 'due');
        $this->assertEquals($this->obj->getQuery('uno'), $dup->getQuery('uno'));
    }
}