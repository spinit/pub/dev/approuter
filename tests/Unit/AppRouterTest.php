<?php

declare(strict_types=1);
namespace Spinit\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\AppRouter;
use Spinit\Dev\AppRouter\Request;
use Spinit\Test\Src\TestInstance;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getenv;

/**
 * @xcoversDefaultClass \Spinit\Dev\AppRouter\AppRouter
 */
class AppRouterTest extends TestCase
{
    /**
     * @var: AppRouter
     */
    private $router;

    public function setUp() :  void {
        $this->router = new AppRouter();
    }

    /**
     * @xcovers ::addRoute
     */
    public function testRouter(): void
    {
        $this->router->addRoute('t1', function($request) {return 'ok 1 '.$request->getPath();});
        $this->router->addRoute('t1/a', function($request) {return 'ok 1.1 '.$request->getPath();});
        $this->router->addRoute('t2', function($request) {return 'ok 2 '.$request->getPath();});

        self::assertEquals('ok 1.1 b', $this->router->run(new Request('GET','t1/a/b')));
        self::assertEquals('ok 1 c/b', $this->router->run(new Request('GET', 't1/c/b')));
        self::assertEquals('ok 2 a/b', $this->router->run(new Request('GET', 't2/a/b')));
    }

    /**
     * @xcovers ::addRoute
     */
    public function testInstance(): void {

        $this->router->addRoute('t1', function($request) {
            $istance = new TestInstance();
            return $istance->run($request);
        });
        self::assertEquals('a', $this->router->run(new Request('GET', 't1/a')));
        self::assertEquals('b/c', $this->router->run(new Request('GET', 't1/b/c')));
    }
}
