<?php
namespace Spinit\Test\Src;

use Spinit\Dev\AppRouter\Core\HasRequestTrait;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;

class TestInstanceOld implements InstanceInterface {
    
    private $response;
    use HasRequestTrait;

    public function run(Request $request, MainInteractorInterface $mi = null)
    {
        $this->setRequest($request);
        return $request->getPath();
    }
    
    public function getDataSource($name = '')
    {
        return;
    }
    public function getResponse()
    {
        if (!$this->response) $this->response = new Response();
        return $this->response;
    }
    public function setUser($user)
    {
        
    }
    public function getUser()
    {
        
    }
    public function hasUser()
    {
        
    }
}