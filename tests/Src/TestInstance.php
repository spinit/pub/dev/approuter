<?php
namespace Spinit\Test\Src;

use Spinit\Dev\AppRouter\Core\HasRequestTrait;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Instance;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;

class TestInstance extends Instance {
    private $main;
    private $response;
    use HasRequestTrait;

    public function getMainInteractor(): MainInteractorInterface
    {
        return $this->main;
    }
    public function run(Request $request, MainInteractorInterface $mi = null)
    {
        $this->setRequest($request);
        $this->main = $mi;
        return $request->getPath();
    }
    
}