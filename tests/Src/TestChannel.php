<?php
namespace Spinit\Test\Src;

use Spinit\Dev\AppRouter\Helper\ChannelInterface;

class TestChannel implements ChannelInterface {
    
    private $content = '';

    function header($code, $headers) {}
    function write($content) {
        $this->content = $content;
    }
    function open() {
        ob_start();
    }
    function close() {
        $this->content .= ob_get_clean();
    }

    function getContent() {
        return $this->content;
    }
}