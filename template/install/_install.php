<?php

use Spinit\Dev\AppRouter\Command\ResponseAddExec;
use Spinit\Dev\AppRouter\Config;
use Spinit\Dev\AppRouter\Core\Console;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Data;
use Spinit\Dev\AppRouter\Entity\MainInteractorDataSource\Installer;
use Spinit\Dev\AppRouter\Helper\ConsoleWamp;
use Webmozart\Assert\Assert;
use Spinit\Dev\AppRouter\Request;
use Spinit\Util\Error\FoundException;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

$result = '';
$interactor = $this->getInstance()->getMainInteractor();
$firstPrefix = '';
// constrollo sui dati
try {
    $istanze = new Data();
    $prefixList = [];

    foreach(Config::getInstanceList() as $name => $info) {
        Assert::notEmpty($prefix = trim(trim($request->getPost(['txt_path', $name]), '/')), 'Punto di accesso non impostato');
        if (in_array($prefix, $prefixList)) {
            throw new FoundException('Punto di accesso duplicato in '.$info->get('label',$name));
        }
        $prefixList[] = $prefix;

        Assert::notEmpty($con_str = trim($request->getPost(['txt_cns', $name])), 'Stringa di connessione non impostata');

        $admin = false;
        if ($info->get('admin')) {
            Assert::notEmpty($usr_name = trim($request->getPost(['txt_nme', $name])), 'Nome utente non impostato');
            Assert::notEmpty($usr_passwd = trim($request->getPost(['txt_pwd', $name])), 'Password non impostata');
            $admin = [
                'name' => $usr_name, 
                'passwd'=> md5($usr_passwd)
            ];
        }
        $con_key = md5($con_str);
        if (!$istanze->has($con_key)) {
            $istanze[$con_key] = ['info'=>$info, 'name'=> $name, 'datasource'=>$con_str, 'managerList'=>[]];
        }
        $istanze[$con_key]['managerList'][] = [
            'manager' => $info->get('manager'),
            'init' => $info->get('init'),
            'theme' => $info->get('theme'),
            'prefix' => $prefix,
            'conf' => trim($request->getPost("txt_conf,".$name)),
            'admin' => $admin
        ];
    }
    Assert::true(count($istanze) > 0, 'Nessun Manager Impostato');

    if (getenv('PHPUNIT')) {
        $result = $interactor->install($istanze);
        return [
            'status'=>'success', 
            'data'=>['exec'=>[
                ['cmd', 'this.closest(".cmd").classList.add("install");']
            ]],
            'result' => $result
        ];
    }

} catch (\Exception $e) {
    
    return [
        'status'=>'error', 
        'data'=>['exec'=>[
            ['cmd', 'desktop.alert(args[0], args[1], args[2])', 'Attenzione',$e->getMessage(), 'error']
        ]],
        'result'=>$e->getMessage()."\n".$e->getTraceAsString()
    ];

}

$console = new ConsoleWamp($interactor);
$response = $this->getInstance()->getResponse();

(new ResponseAddExec($response))->exec("this.closest('.cmd').classList.add('install')");
// try avvio
// il processo installazione è già attivo?
if ($fp = @fopen(Installer::SEMAPHORE, 'x')) {
    // viene chiuso ma il file rimane visibile
    fwrite($fp, getmypid());
    fclose($fp);
    // registrazione sblocco semaforo a fine processo
    register_shutdown_function(function() use ($console) { 
        unlink(Installer::SEMAPHORE);
        // comunico la fine del processo
        $console->log('End process : '.date('H:i:s'), ob_get_clean());
    });
    // switch console
    $this->getInstance()->getMainInteractor()->setConsole($console);

    $response->then(function() use ($istanze, &$prefixList, $console) {
        try {
            ob_start();
            // avvio installazione dopo la chiusura della connessione con il browser
            $console->log('Init process : '.date('H:i:s'));
            $this->getInstance()->getMainInteractor()->install($istanze);
            $console->info(['exec'=>[['cmd', 'location.replace(args[0])', '//'.$prefixList[0]]]]);
        } catch (\Exception $e) {
            $console->error($e->getMessage(), $e->getTraceAsString());
        }
    });
}

return $response;
