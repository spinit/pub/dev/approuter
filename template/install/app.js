var connection = new autobahn.Connection({url: 'ws://'+location.hostname+':'+location.port+'/ws', realm: 'realm1'});

connection.onopen = function (session) {
   function write(message, type) {
      let d = document.createElement('div');
      let console = document.querySelector('.console');
      d.classList.add(type + '');
      d.innerText = message;
      console.appendChild(d);
      console.scrollTop = console.scrollHeight;
   }

   session.subscribe('console.log', function onevent(args) {
      write(JSON.stringify(args));
   });
   session.subscribe('console.error', function onevent(args) {
      write(JSON.stringify(args), 'error');
   });
   session.subscribe('console.info', function onevent(args) {
      if(args[0] && args[0].exec) {
         desktop.exec(args[0].exec);
      } else {
         console.log("Info : ", args);
      }
   });
};

connection.open();