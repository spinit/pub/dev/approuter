(function() {

    // gestione visualizzazione spinner
    let spinner = new (function() {
        let body = document.querySelector('body');
        this.start = ()=>body.classList.add('view-spinner');
        this.ent = ()=>body.classList.remove('view-spinner');
    })();

    // classe desktop
    let desktop = {
        alert : function(title, message, classer) {
            desktop.log(arguments);
            alert(message);
            return desktop;
        },
        error : function () {
            console && console.error && console.error.apply(console, [].slice.call(arguments));
            return desktop;
        },
        log :  function() {
            console && console.log && console.log.apply(console, [].slice.call(arguments));
            return desktop;
        },
        exec : function(list, target, response) {
            Array.from(list).forEach((item)=>{
                if (!item) return;
                if (typeof(item) == typeof('')) {
                    item = [item];
                }
                if (item.length < 2) {
                    item.unshift('cmd');
                }
                var type = item.shift();
                switch(type) {
                    case 'cmd' : 
                        var code = item.shift();
                        var cmd = new Function('args, resp', code);
                        cmd.apply(target ? target : window, [item, response]);
                        break;
                }
            });
        },
        on : function (events, selector, callback) {
            let listener = selector instanceof Function ? selector : callback;
            let filter   = selector instanceof Function ? '' : selector;
            if (!filter) filter = 'body';
            events.trim().split(' ').forEach((ev) =>
                document.addEventListener(ev, (event) => {
                    let element = event.target.closest(filter);
                    if (element) listener.apply(element, Array.from (arguments))
                }, false)
            );
        },
        trigger : function(element, event, parma) {
            if (element instanceof String) {
                element = document.querySelector(element);
            }
            if (event instanceof String) {
                event = new Event(event);
            }
            element.dispatchEvent(event);
        }
    };

    desktop.on('submit', 'form', () => false);

    desktop.on('click', "[osy-event]", function() {
        console.log(this);
        let target = this;
        let href = target.getAttribute('osy-href') || location.href;
        let form = target.closest('form');
        let data = new FormData(form);
        
        data.set('_[osy][event]', target.getAttribute('osy-event'));

        target.getAttribute('osy-param') && 
        data.set('_[osy][param]', target.getAttribute('osy-param'));

        spinner.start();
        fetch(href, {'method':'POST', 'body': data})
        .then((response) => response.json())
        .then(function(response) {
            let message = response.message;
            if(message) {
                if (message instanceof Array) {
                    message = ['Attenzione', $message];
                }
                desktop.alert.apply(desktop, message);
            } 
            
            response.data && 
            response.data.exec && 
            desktop.exec(response.data.exec, target, response);
        })
        .catch((error) => desktop.error(error))
        .finally(spinner.end);
    });
    window['desktop'] = desktop;
})();