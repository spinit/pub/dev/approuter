<?php

use Spinit\Dev\AppRouter\Config;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

include_once dirname(__DIR__).'/lib.php';
$this->getInstance()->getResponse()->setType('html');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="<?php mk($this, 'style.css')?>" rel="stylesheet"/>
    <title>Hello, world!</title>
  </head>
  <body class="main">
  <form method="POST">
<?php
    $cc = 0;
    foreach(Config::getInstanceList() as $idx => $item) {
        $cc += 1;
?>
            <div class="manager">
                <div>
                    <h4>Instance : <?php echo $item->get('label', $idx)?></h4>
                    <label>Access Point</label>
                    <input id="txt_path_<?php echo $idx?>" name="txt_path[<?php echo $idx?>]" value="<?php echo ltrim($this->getInstance()->makePath($item->get('slug')), '/')?>"/>
                    <label>Connection String</label>
                    <input id="txt_cns_<?php echo $idx?>" name="txt_cns[<?php echo $idx?>]" value="<?php echo $item->get('constr')?>"/>
                    <label>Configuration String</label>
                    <input id="txt_conf_<?php echo $idx?>" name="txt_conf[<?php echo $idx?>]"/>
                    <?php if ($item->get('admin')) {?>
                    <hr/>
                    <label>User Admin</label>
                    <input id="txt_nme_<?php echo $idx?>" name="txt_nme[<?php echo $idx?>]"/>
                    <label>Password</label>
                    <input id="txt_pwd_<?php echo $idx?>" name="txt_pwd[<?php echo $idx?>]"/>
                    <?php } ?>
                </div>
                <div>
                </div>
            </div>
<?php
    }
    if ($cc){
        $class = '';
        $semaphore = '/tmp/osy-install.semaphore';
        if (is_file($semaphore)) {
            $class = 'install';
        }
?>
        <div class="cmd">
            <div class="button">
                <button class="btn btn-success" osy-event="install" type="button">Installa</button>
            </div>
            <div class="console"></div>
        </div>
<?php
    }
    else {
        ?>
        <div class="error">
            NESSUN MANAGER IMPOSTATO
        </div>
<?php
    }
?>
        </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script 
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
        crossorigin="anonymous"></script>
    <script 
        src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
        crossorigin="anonymous"></script>

        <script src="<?php mk($this, 'wamp/js/autobahn.js')?>"></script>
        <script src="<?php mk($this, 'desktop.js')?>"></script>
        <script src="<?php mk($this, 'app.js')?>"></script>
  </body>
</html>
