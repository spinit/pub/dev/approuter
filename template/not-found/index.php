<?php
include_once dirname(__DIR__).'/lib.php';
$this->getInstance()->getResponse()->setType('html');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="<?php mk($this, 'style.css')?>" rel="stylesheet"/>
    <title>AppRoute : INSTANCE NOT FOUND</title>
  </head>
  <body class="main">
    <div class="message" osy-instance="not-found">INSTANCE NOT FOUND</div>
  </body>
</html>
