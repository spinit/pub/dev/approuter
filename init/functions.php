<?php
namespace Spinit\Dev\AppRouter;

use Spinit\Util;

use function Spinit\Util\getenv;

function getMimeType($idx) {

    $mimet = array(
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        // font
        'woff'=>'application/font-woff',
        'woff2'=>'font/woff2',
    );

    if (isset($mimet[$idx])) {
        return $mimet[$idx];
    } else {
        return '';
    }
}

function fspath() {
    $params = func_get_args();
    $args = array_map(function($item, $k=0) {
        static $k = 0;
        return str_replace('/', DIRECTORY_SEPARATOR, !$k++?$item:ltrim($item, '/'));
    }, array_filter($params, 'trim'));
    $path = str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $args));
    return $path;
}

function debug()
{
    if (!getenv('PHPUNIT')) {
        header('Content-type: text/plain');
    }
    
    $e = new \Exception();

    foreach ([0, 1, 2, 3 , 4, 5, 6,7,8] as $k) {
        if ($trace = Util\arrayGet($e->getTrace(), $k)) {
            if (Util\arrayGet($trace, 'file')) {
                echo "== ".Util\arrayGet($trace, 'file').':'.Util\arrayGet($trace, 'line')."\n";        
            }
        }
    }
    var_dump(func_get_args());
    exit;
}

function loadConfig($object) 
{
    $args = func_get_args();
    array_shift($args);
    if (is_string($object)) {
        $info = pathinfo($object);
    } else {
        $rec = new \ReflectionObject($object);
        $info = pathinfo($rec->getFileName());
    }
    $par1 = array_shift($args);
    $fcnt = null;
    if (is_callable($par1)) {
        $fcnt = $par1;
        $ext = array_shift($args);
    } else {
        $ext = $par1;
    }
    $ext = $ext ?: 'xml';
    $cfile = $info['dirname'].'/'.$info['filename'].'.'.$ext;
    
    if (array_shift($args)) {
        debug($info, $cfile, is_file($cfile));
    }
    $cnt = file_get_contents($cfile);
    
    if ($fcnt) {
        $cnt = call_user_func($fcnt, $cnt);
    }
    $data = [];
                
    if (is_file($cfile)) {
        switch($ext) {
            case 'xml':
                $data = json_decode(json_encode(simplexml_load_string($cnt , null, LIBXML_NOCDATA)), 1);
                break;
            case 'json':
                $data = json_decode($cnt, 1);
                break;
        }
    }
    return new Data($data);
}